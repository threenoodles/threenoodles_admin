/**
* @FileName: SessionParams.java
* @Package com.rotek.constant
* @Description: TODO
* @author chenwenpeng
* @date 2013-7-19 下午03:14:45
* @version V1.0
*/
package com.rotek.constant;

/**
 * @ClassName: SessionParams
 * @Description: session中存放的参数
 * @author chenwenpeng
 * @date 2013-7-19 下午03:14:45
 *
 */
public class SessionParams {

	/**@Field the String USER 登陆用户信息*/
	public static final String USER = "user";
}

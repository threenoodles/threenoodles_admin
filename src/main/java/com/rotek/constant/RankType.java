/**
* @FileName: RankType.java
* @Package com.rotek.constant
* @Description: TODO
* @author chenwenpeng
* @date 2013-9-3 上午11:51:24
* @version V1.0
*/
package com.rotek.constant;

/**
 * @ClassName: RankType
 * @Description:
 * @author chenwenpeng
 * @date 2013-9-3 上午11:51:24
 *
 */
public class RankType {

	/**@Field the int DEFAULT 默认级别*/
	public static final int DEFAULT = 0;

	/**@Field the int HEIGHT 高级级别*/
	public static final int HEIGHT = 1;
}
